## Pre-requisities
Runs on Node versions >= 10

Install dependencies using npm:
```
npm run install-pkg
```

## How to run
Run the starter the Express server and the react client:
```
npm run dev
```

## Notes
**ts-node-dev** is the Typescript replacement for Nodemon. It allows us to run the ts file directly. This is to avoid having to stop the server to run tsc && node ./index.js

## License
MIT

## Walkthrough
I opted for the db solution. In real life, the data would be much larger.

The input queries the db which returns an array of results (could be empty).
Those results are all sent back to the client, and the client diplays them (not all at once, but through a scroll, sort of like a pagination).

The client can select in which property field the query must be made.

I wrote a short (and very imperfect) client side validation for fun.
