import Layout from "../components/ui/Layout";
import LayoutLimiter from "../components/ui/LayoutLimiter";
import Button from '../components/ui/Button';
import { FormEvent, useEffect, useRef, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { postData } from "src/utils";
import { SpinnerCircular, SpinnerInfinity } from "spinners-react";

export const App: React.FC = () => {

	const initialValues = {
		'search_field': 'all',
		'search_keyword': ''
	};

	// react state
	const [values, setValues] = useState<{[key: string]: any}>(initialValues);
	const [patents, setPatents] = useState<Array<any>>([]);
	const [patentsToShow, setPatentsToShow] = useState<Array<any>>([]);

	const [isLoading, setIsLoading] = useState(false);
	const [hasSubmitted, setHasSubmitted] = useState(false);
	const [errors, setErrors] = useState<{[key: string]: string}>({});

	const form = useRef<HTMLFormElement>(null)

	function handleChange(event: React.ChangeEvent<HTMLInputElement>) {

		removeError(event.target);

		const value = event.target.type === "checkbox" ? event.target.checked : event.target.value;

		setValues({
			...values,
			[event.target.name]: value
		});
	};

	// form submission, fetch data
	const handleSubmit = (event: FormEvent<HTMLFormElement>) => {

		event.preventDefault();

		let target = event.target as HTMLFormElement;

		checkErrors(target, true);

		if ( Object.keys(errors).length !== 0 ) {
			return;
		}

		setPatents([]);
		setPatentsToShow([]);

		let loadTimer = setTimeout(() => {

			setIsLoading(true);
		}, 500);

		postData('/api/chemicals', values)
		.then(data => {

			let d = data.data;
			setPatents(d);
			clearTimeout(loadTimer);
			setIsLoading(false);
			setHasSubmitted(true);

		})
		.catch(e => {

			clearTimeout(loadTimer);
			setIsLoading(false);
			console.log(e);
		});
	};

	const validation = {
		'search_keyword': {
			required: true,
			minLength: 4
		}
	};

	// validation
	const handleValidation = (event: React.FocusEvent<HTMLInputElement>) => {

		var input = event.target;

		validate(input);
		showError(input);
	};

	const validate = (input: HTMLInputElement) => {

		Object.entries(validation).forEach(function (a) {

			var key = a[0]
			var properties = a[1];

			if ( key !== input.name ) { return; }

			if ( properties.required && input.value.length === 0) {
				addError(input, 'Required');
				return;
			}

			if ( properties.minLength && input.value.length < properties.minLength ) {
				addError(input, `Min length is: ${properties.minLength}`);
				return;
			}

		});
	}

	function removeError(input:HTMLInputElement) {

		var temp = JSON.parse(JSON.stringify(errors));
		delete temp[input.name];
		setErrors(temp);

		var field = input.closest('.field');

		if ( field ) {
			field.classList.remove('has-error');
		}

		var message = field?.querySelector('div.message');

		if ( message )  {
			message.textContent = '';
		}
	};

	function addError(input:HTMLInputElement, message: string) {

		setErrors({
			...errors,
			[input.name]: message
		})

		var field = input.closest('.field');

		if ( field ) {
			field.classList.add('has-error');
		}

		var msg = field?.querySelector('div.message');

		if ( msg )  {
			msg.textContent = message;
		}
	};

	function showError(input:HTMLInputElement) {

		var field = input.closest('.field');
		field?.classList.add('touched');

	}

	function checkErrors(form: HTMLFormElement, withError = false) {

		Object.entries(validation).forEach(function(i) {

			let name = i[0];

			var input = form.querySelector(`input[name="${name}"]`) as HTMLInputElement | undefined;

			if ( input ) {
				validate(input);
				if ( withError ) {

					showError(input);
				}
			}

		});
	};

	// scroller logic
	const fetchMoreData = () => {

		setPatentsToShow(patents.slice(0, patentsToShow.length - 1 + 5 >= patents.length ? patents.length : patentsToShow.length - 1 + 5));
	};

	useEffect(() => {

		if ( form.current ) {
			checkErrors(form.current);
		}

	}, [form.current]);

	useEffect(() => {

		if ( patents.length !== 0 ) {
			setPatentsToShow(patents.slice(0, 4));
		}

		return () => {
			setPatentsToShow([]);
		}

	}, [patents]);

	// ui
	return (
		<Layout>
			<LayoutLimiter>

				<form autoComplete="off" onSubmit={handleSubmit} ref={form}>

					<ul className="fields">

						<li className="field radio reverse">
							<input type="radio" name="search_field" id="all" value="all" onChange={handleChange} defaultChecked={true}/>
							<label htmlFor="all">all</label>
							<div className="radiomark"></div>
						</li>
						<li className="field radio reverse">
							<input type="radio" name="search_field" id="patent_no" value="patent_no" onChange={handleChange}/>
							<label htmlFor="patent_no">patent no</label>
							<div className="radiomark"></div>
						</li>
						<li className="field radio reverse">
							<input type="radio" name="search_field" id="patent_title" value="patent_title" onChange={handleChange} />
							<label htmlFor="patent_title">patent title</label>
							<div className="radiomark"></div>
						</li>
						<li className="field radio reverse">
							<input type="radio" name="search_field" id="chemical_type" value="chemical_type_1" onChange={handleChange} />
							<label htmlFor="chemical_type">chemical type</label>
							<div className="radiomark"></div>
						</li>

						<li className="field duo floating-label">
							<input type="text" id="keyword" name="search_keyword" placeholder="Search..." onChange={handleChange} onBlur={handleValidation}/>
							<label htmlFor="keyword">Search...</label>
							<div className="message"></div>
						</li>
						<li className="field duo">
							<Button type="submit">Search</Button>
						</li>

					</ul>

				</form>

				{ isLoading && <SpinnerCircular />}

				{ patents.length !== 0 && values.search_field === 'chemical_type_1' && <div className="chemical-amount">{patents[0]['chemical type 1']}: {patents.length}</div> }

				{ patentsToShow.length > 0
				&& <InfiniteScroll
					dataLength={patentsToShow.length}
					next={fetchMoreData}
					hasMore={patentsToShow.length !== patents.length}
					loader={<SpinnerInfinity />}
				>
					{patentsToShow.map((p, i) => (
						// using index for key is very bad practice but the data has no id, and I am too lazy to create a hash based on the title. There are duplicated in the db
						<div key={p['patent no'] + '- ' + p['chemical type 1'].split(' ').join('-') + '-' + i } className="patent-info">
							<div><span className="highlight">Patent Title: </span>{p['patent title'].toLowerCase()}</div>
							<div><span className="highlight">Chemical Type: </span>{p['chemical type 1']}</div>
							{/* for some reason there ia an extra zero after the irst 4 figures */}
							<div>
								<span className="highlight">Patent Number: </span>
								<a
									href={`https://patents.google.com/patent/${p['patent no'].slice(0, 6) + '0' + p['patent no'].slice(6)}`}
									target="_blank"
									rel="noopener noreferrer"
								>
									{p['patent no'].slice(0, 6) + '0' + p['patent no'].slice(6)}
								</a>
							</div>
						</div>
					))}
				</InfiniteScroll>
				}

				{ Object.keys(values).length !== 0 && patents.length === 0 && hasSubmitted && <div>Nothing found</div>}

			</LayoutLimiter>
		</Layout>
	)
}


export default App;
