
interface LayoutLimiterProps {
	children?: any;
	className?: string,
}

export const LayoutLimiter: React.FC<LayoutLimiterProps> = ({ children, className = '' }) => {

	return (
		<section className={`limiter ${className}`}>
			{children}
		</section>
	)
}
export default LayoutLimiter
