import { Fragment } from "react"
import Header from "./Header";
import { Footer } from './Footer';
import { MainContainer as Main } from "./MainContainer";

export const Layout: React.FC = ( { children }: any) => {

	return (
		<Fragment >

			<Header />

			<Main>

				{ children }

			</Main>


			<Footer />

		</Fragment>
  )
}

export default Layout;
