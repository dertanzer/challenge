
import { LayoutLimiter } from './LayoutLimiter';
interface FooterProps {
}

export const Footer: React.FC<FooterProps> = () => {

	return (
		<footer>
			<LayoutLimiter>
				Ledanseur Hadrien &amp; Knowledge Pioneer
			</LayoutLimiter>
		</footer>
	)
}

export default Footer
