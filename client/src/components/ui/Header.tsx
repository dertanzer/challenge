
import { LayoutLimiter } from 'src/components/ui/LayoutLimiter';

interface HeaderProps {
}

export const Header: React.FC<HeaderProps> = () => {

	return (
		<nav>
			<LayoutLimiter className="wrapper">
				<a href="https://www.chemovator.com/" target="_blank" rel="noreferrer">
					<img src="ic-chemovator-logo.svg" alt="chemovator" className="logo" />
				</a>
			</LayoutLimiter>
		</nav>
	)
}
export default Header