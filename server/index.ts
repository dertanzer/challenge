import express, { Application, Request, Response } from "express";
import cors from "cors";
import { getChemicals } from './api/chemicals/post';

const app: Application = express();
const port = 7777;

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors())

app.post(
	"/api/chemicals",
	async (req: Request, res: Response): Promise<Response> => {

		return getChemicals(req, res);
	}
);

try {
	app.listen(port, (): void => {
		console.log(`Connected successfully on port ${port}`);
	});

} catch (error: unknown) {

	console.error(`Error occured: ${error}`);
}
