import { Request, Response } from "express";
import { getFileData, search } from '@utils/index';

type Chemicals = {
	"patent no": string;
	"patent title": string;
	"chemical type 1": string;
};

const MIN_SEARCH_WORD_LENGTH = 4;

let chemicals: Chemicals = getFileData('./server/data/chemical_type_1.json');

export const getChemicals = async (req: Request, res: Response) => {

	if ( req.body.search_keyword === undefined || req.body.search_keyword < MIN_SEARCH_WORD_LENGTH ) {

		return res.status(400).send({
			error: 'SEARCH_KEYWORD_TOO_SHORT'
		});

	}

	let searchFields = req.body.search_field && req.body.search_field !== 'all' ? [ req.body.search_field.split('_').join(' ') ] : [ 'patent no', 'patent title', 'chemical type 1' ];

	var searchResults = search( searchFields, req.body.search_keyword, chemicals, MIN_SEARCH_WORD_LENGTH );

	if ( searchResults.length === 0 ) {

		return res.status(200).send({
			data: []
		});

	}

	const limit = parseInt(req.body.limit);
	const skip = parseInt(req.body.skip);

	if ( skip ) {
		searchResults?.splice(skip, searchResults.length);
	}

	if ( limit ) {
		searchResults?.splice(0, limit);
	}

	return res.status(200).send({
		data: searchResults
	});
}